<?php

/**
 *
 * Plugin Name: Buildy for WP
 * Plugin URI:
 * Description: Just like a chocolate milk, only apples!
 * Version:     2.1.3
 * Author:
 * Author URI:
 * Text Domain: buildy-wp
 * Bitbucket Plugin URI: https://johnpieters@bitbucket.org/hmwinternalprojects/buildy-wp.git
 *
 */

if (!defined('ABSPATH')) {
    die('Invalid request.');
}

// Load Composer
require_once('vendor/autoload.php');

// Init Buildy
use Michaelr0\BuildyWp\Buildy;

//global $buildy;
$buildy = (new Buildy);

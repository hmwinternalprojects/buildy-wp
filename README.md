# Buildy for WP

Buildy for WP is a project born out of frustration with various technical and perforamce related shortcomings experienced with Divi, Buildy is a "Page Builder" of sorts, using VueJS for the backend and Blade templating for the frontend.

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email michael@rook.net.au instead of using the issue tracker.

## Credits

- [Michael Rook](https://github.com/michaelr0)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
